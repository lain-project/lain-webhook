const { Pool } = require("pg");

const database = async() => {
  const connectionString = process.env.DATABASE_URL;
  const pool = new Pool({
    connectionString,
    application_name: "$ docs_simplecrud_node-postgres",
  });

  // Connect to database
  const client = await pool.connect();
  const query = `SELECT * FROM auditoria`;

  try{
  const auditoria = await client.query(query)
  console.log(auditoria)
  return auditoria
  } catch (error) {
      console.error(error)
  }
}

module.exports = {database}